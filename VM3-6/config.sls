# Configuration eth1
# RAPPEL: eth0 est à vagrant, ne pas y toucher

## Désactivation de network-manager
NetworkManager:
  service:
    - dead
    - enable: False
    
## Suppression de la passerelle par défaut
ip route del default:
  cmd:
    - run

##Configuration de VM3
eth1:
  network.managed:
    - enabled: True
    - type: eth
    - proto: none
    - enable_ipv4: false
    - ipv6proto: static
    - enable_ipv6: true
    - ipv6_autoconf: no
    - ipv6ipaddr: fc00:1234:2::36
    - ipv6netmask: 64

eth2:
  network.managed:
    - enabled: True
    - type: eth
    - proto: none
    - ipaddr: 172.16.2.186
    - netmask: 28

## But enable ipv4 forwarding
net.ipv4.ip_forward:
  sysctl:
    - present
    - value: 1

ip route add fc00:1234:1::/64 via fc00:1234:2::26:
  cmd:
    - run

el_tunel_systemd_unit:
  file.managed:
    - name: /etc/systemd/system/el-tunel.service
    - source: salt://el-tunel.service

el_tunel_running:
  service.running:
    - name: el-tunel
    - enable: True
    - watch:
      - file: el_tunel_systemd_unit

ip route add 172.16.2.144/28 via 172.16.2.1:
  cmd:
    - run

