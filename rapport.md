2.2. Configuration de l’interface

2. Il faut à présent indiquer à la VM1 de passer par VM1-6 pour accéder à LAN4.
   Il faut également changer le routage de VM1-6 et lui indiquer que les paquets à destination de LAN4 doivent êtres redirigés vers tun0.
3. Quand on fait un ping sur 172.16.2.1, on ne lit rien sur tun0 avec Wireshark.
4. Quand on fait un ping sur 172.16.2.10, on peut à présent lire les paquets qui passent par tun0 avec Wireshark.
5. Quand on ping sur 172.16.2.1 on se ping soi-même. La pile réseau va alors utiliser l'interface LO, c'est pour cela que l'on observe rien sur tun0.
   Quand on ping sur 172.16.2.10 par contre on va passer par tun0 (car c'est une adresse du même sous-réseau) et on pourra observer le traffic.

2.3 Récupération des paquets

3. On peut afficher dans le terminal les données qui transitent sur tun0 grâce à la fonction copy() de notre bibliothèque iftun:
```
00000000  00 00 08 00 45 00 00 54  c4 43 40 00 40 01 1a 3a  |....E..T.C@.@..:|
00000010  ac 10 02 01 ac 10 02 0a  08 00 bf 1c 09 1a 00 01  |................|
00000020  5b 0d af 5d 00 00 00 00  63 8a 03 00 00 00 00 00  |[..]....c.......|
00000030  10 11 12 13 14 15 16 17  18 19 1a 1b 1c 1d 1e 1f  |................|
00000040  20 21 22 23 24 25 26 27  28 29 2a 2b 2c 2d 2e 2f  | !"#$%&'()*+,-./|
00000050  30 31 32 33 34 35 36 37                           |01234567|
00000058
```
En comparant avec Wireshark, on s'appercoit que 4 octets ont étés rajoutés devant les données du paquet.
```
00000000  45 00 00 54 2e 55 40 00  40 01 b0 28 ac 10 02 01  |E..T.U@.@..(....|
00000010  ac 10 02 0a 08 00 61 68  0a 12 00 01 97 11 af 5d  |......ah.......]|
00000020  00 00 00 00 80 42 07 00  00 00 00 00 10 11 12 13  |.....B..........|
00000030  14 15 16 17 18 19 1a 1b  1c 1d 1e 1f 20 21 22 23  |............ !"#|
00000040  24 25 26 27 28 29 2a 2b  2c 2d 2e 2f 30 31 32 33  |$%&'()*+,-./0123|
00000050  34 35 36 37                                       |4567|
00000054
```
4. En rajoutant le drapeau IFF_NO_PI, on désactive l'ajout des quatre octets par le noyau, et on obtient le même résultat qu'avec Wiresherk.
Il faudra donc laisser activée cette option dans le futur.
