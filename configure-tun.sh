#!/bin/bash

echo "Configuration de tun0 en 172.16.2.1/28"
ip link set tun0 up
ip addr add 172.16.2.1/28 dev tun0
